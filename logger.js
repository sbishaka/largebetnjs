/**
 * Created by Bishaka on 05/04/2016.
 */
(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
        typeof define === 'function' && define.amd ? define(['exports'], factory) :
            (factory((global.logger = global.logger || {})));
}(this, function (exports) {
    'use strict';
    var _logger = require('winston'),
        moment = require('moment'),
        Promise = require("bluebird"),
        delim = " | ";

    _logger.remove(_logger.transports.Console);
    _logger.add(_logger.transports.Console, {colorize: true});

    var log = function (_opts) {
            return new Promise(function (resolve, reject) {
                var opts = _opts || {},
                    timestamp = moment().toISOString(),
                    level = opts["level"],
                    msg = [ timestamp,
                            level,
                            opts["file"] || "",
                            opts["func"] || "",
                            opts["id"] || "",
                            opts["msg"]
                            ].join(delim);
                _logger.log(level,msg);
                resolve(opts);
            });
     };

    exports.log = log;
    exports.engine = _logger;
}));