/**
 * Created by Bishaka on 05/04/2016.
 */
(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
        typeof define === 'function' && define.amd ? define(['exports'], factory) :
            (factory((global.lbsm = global.lbsm || {})));
}(this, function (exports) {
    'use strict';
    var lbsts = require('./largebetStates'),
        moment = require('moment'),
        logger = require('./logger'),
        Promise = require("bluebird"),
        //agencycodes_url = "http://bt-agencycodes.rhcloud.com/api/binds",
        agencycodes_url = "http://localhost:8081/api/binds",
        request = require('request'),
        db = require('./db').db,
        ttable = []; //Stores which state transitions to which

    var getFilename = function () {
        var src = "";
        try{
            throw new Error();
        }catch(err){
            var regex = /\(.*\)/,
                match = regex.exec(err.stack),
                name = match[0].replace("(","").replace(")",""),
                tokens = name.split(require('path').sep);
            src = tokens[tokens.length-1].split(":")[0];
        }
        return src;
    };

    var buildTransitionTable = function () {
        var trow = {};
        trow[lbsts.STATE_NAMES.categories]=lbsts.STATE_NAMES.matches;
        trow[lbsts.STATE_NAMES.matches]=lbsts.STATE_NAMES.prediction;
        trow[lbsts.STATE_NAMES.prediction]=lbsts.STATE_NAMES.checkout;
        trow[lbsts.STATE_NAMES.checkout]=lbsts.STATE_NAMES.amount;
        trow[lbsts.STATE_NAMES.amount]=lbsts.STATE_NAMES.placebet;
        trow[lbsts.STATE_NAMES.bet_types]=lbsts.STATE_NAMES.jp_b1;
        trow[lbsts.STATE_NAMES.jp_b1]=lbsts.STATE_NAMES.jp_b2;
        trow[lbsts.STATE_NAMES.jp_b2]=lbsts.STATE_NAMES.jp_b3;
        trow[lbsts.STATE_NAMES.jp_b3]=lbsts.STATE_NAMES.amount;
        trow[lbsts.STATE_NAMES.fp_pb]=lbsts.STATE_NAMES.fp_6hw_b1;
        trow[lbsts.STATE_NAMES.fp_6hw_b1]=lbsts.STATE_NAMES.fp_6hw_b2;
        trow[lbsts.STATE_NAMES.fp_6hw_b2]=lbsts.STATE_NAMES.fp_6hw_b3;
        trow[lbsts.STATE_NAMES.fp_6hw_b3]=lbsts.STATE_NAMES.amount;
        trow[lbsts.STATE_NAMES.fp_4dr_b1]=lbsts.STATE_NAMES.fp_4dr_b2;
        trow[lbsts.STATE_NAMES.fp_4dr_b2]=lbsts.STATE_NAMES.fp_4dr_b3;
        trow[lbsts.STATE_NAMES.fp_4dr_b3]=lbsts.STATE_NAMES.amount;
        trow[lbsts.STATE_NAMES.fp_5aw_b1]=lbsts.STATE_NAMES.fp_5aw_b2;
        trow[lbsts.STATE_NAMES.fp_5aw_b2]=lbsts.STATE_NAMES.fp_5aw_b3;
        trow[lbsts.STATE_NAMES.fp_5aw_b3]=lbsts.STATE_NAMES.amount;
        ttable.push(trow);
    };

    var getFwdTransition = function(prevState){
        var arr = ttable.filter(function(state){
            return state.hasOwnProperty(prevState);
        });
        return arr[0][prevState];
    };

    var getRvsTransition = function (prevState) {
        var result = "";
        var arr = ttable.filter(function(state){
            var keys = Object.keys(state);
            for (var i = 0; i < keys.length; i++) {
                var key = keys[i];
                if(state[key]===prevState){
                    result = key;
                    break;
                }
            }
        });
        return result;
    };

    var getLastState = function (_opts) {
        return new Promise(function (resolve, reject) {
            var opts = _opts || {};
                db["txns"].findOne({"user.se":opts["user"]["se"]}).sort({"_sys_timestamps.created_at":-1}).exec(function(err,doc){
                    opts["lastState"]= doc;
                    if(opts["lastState"]){
                        console.log("Current State Fetched : " + opts["lastState"]["state"]["name"]);
                        console.log("Checking for ticket in last state");
                        if(opts["lastState"]["ticket"]){
                            console.log("Ticket found");
                            opts["ticket"]=opts["lastState"]["ticket"];
                            console.log("Setting current state ticket to : " + JSON.stringify(opts["ticket"]));
                        }else{
                            console.log("Ticket not found");
                        }
                    }else{
                        console.log("Current State Fetched : " + "Not found");
                    }
                    resolve(opts);
                })
        });
    };

    var updateLastStateInDb = function (_opts) {
        return new Promise(function (resolve, reject) {
            var opts = _opts || {}, id = opts["lastState"]["_id"], tstamp = moment().toISOString();
                if(!opts["input_wrong"]){
                db["txns"].update({"_id":id},{$set:{"user.in":opts["user"]["in"],"_sys_timestamps.input_updated_at":tstamp}},{},function(){
                    resolve(opts);
                })}else{
                    resolve(opts);
                }
        });
    };

    var transition = function (_opts) {
        return new Promise(function (resolve, reject) {
            var opts = _opts || {},
                input = parseInt(opts["user"]["in"]),
                logopts = {level:"debug",file:getFilename(),func:"transition", id:"[user_sess]["+opts["user"]["se"]+"]"};

            logopts.msg="Verifying input : " + input;
            logger.log(logopts);

            if(isNaN(opts["user"]["in"])){
                console.log("Input : " + opts["user"]["in"]);

                if(opts["lastState"]["state"]["name"]===lbsts.STATE_NAMES.bet_types){
                    var phone = opts["lastState"]["user"]["no"], code =  opts["user"]["in"];
                    request({
                        url:agencycodes_url,
                        method:"POST",
                        form:{
                            phone:phone,
                            code:code
                        }
                    });
                }

                console.log("Input failed verification test : ");
                opts["input_wrong"]=true;
                opts["state"] = opts["lastState"]["state"];
                console.log("Setting next state to : " + opts["state"]["name"]);
                resolve(opts);
                return;
            }

            switch(opts["lastState"]["state"]["name"]){

                case lbsts.STATE_NAMES.amount:
                    if(input < 1000){
                        logopts.msg="Input failed verification test";
                        logger.log(logopts);

                        opts["input_wrong"]=true;
                        opts["state"] = opts["lastState"]["state"];

                        logopts.msg="Setting next state to : " + opts["state"]["name"];
                        logger.log(logopts);
                        resolve(opts);
                    }else{
                        opts["ticket"] = opts["lastState"]["ticket"] || {};
                        opts["ticket"]["amount"] = input;
                        opts["state"] = {"name":lbsts.STATE_NAMES.checkout};
                        logopts.msg="Transitioning from [" + lastState + "] to [" + opts["state"]["name"] +"]";
                        logger.log(logopts);
                        lbsts.exec(opts)
                            .then(resolve);
                    }
                break;
                case    lbsts.STATE_NAMES.fp_6hw_b1:
                case    lbsts.STATE_NAMES.fp_6hw_b2:
                case    lbsts.STATE_NAMES.fp_6hw_b3:
                case    lbsts.STATE_NAMES.fp_5aw_b1:
                case    lbsts.STATE_NAMES.fp_5aw_b2:
                case    lbsts.STATE_NAMES.fp_5aw_b3:
                case    lbsts.STATE_NAMES.fp_4dr_b1:
                case    lbsts.STATE_NAMES.fp_4dr_b2:
                case    lbsts.STATE_NAMES.fp_4dr_b3:
                    var lastState = opts["lastState"]["state"]["name"],
                        nextState =  getFwdTransition(lastState);
                    opts["state"] = {"name":nextState};
                    console.log("Setting next state to : " + opts["state"]["name"]);
                    opts["upd8ticket"] = lastState.split("_")[1].trim();
                    lbsts.upd8ticket(opts)
                        .then(lbsts.exec)
                        .then(resolve);
                break;
                case    lbsts.STATE_NAMES.jp_b1:
                case    lbsts.STATE_NAMES.jp_b2:
                case    lbsts.STATE_NAMES.jp_b3:
                    var lastState = opts["lastState"]["state"]["name"],
                        nextState =  getFwdTransition(lastState);

                    opts["state"] = {"name":nextState};
                    console.log("Setting next state to : " + opts["state"]["name"]);
                    if(lastState === lbsts.STATE_NAMES.jp_b3){
                        opts["upd8ticket"] = "uo"
                        lbsts.upd8ticket(opts)
                            .then(lbsts.exec)
                            .then(resolve)
                    }else{
                        lbsts.exec(opts)
                            .then(resolve);
                    }
                break;
                default:
                    if(input < 0 || input > opts["lastState"]["state"]["entries"].length){
                        console.log("Input failed verification test");
                        opts["input_wrong"]=true;
                        opts["state"] = opts["lastState"]["state"];
                        console.log("Setting next state to : " + opts["state"]["name"]);
                        resolve(opts);
                    }else{
                        logopts.msg="Input passed verification test";
                        logger.log(logopts);

                        var lastState = opts["lastState"]["state"]["name"],
                            nextState =  getFwdTransition(lastState),
                            entry = opts["lastState"]["state"]["entries"][input-1];

                        logopts.msg="Checking entry : " + entry;
                        logger.log(logopts);

                        switch(entry){
                            case lbsts.ENTRIES.back:
                                var prevState = getRvsTransition(lastState);
                                opts["state"] = {"name":prevState};

                                logopts.msg="Transitioning from [" + lastState + "] to [" + prevState +"]";
                                logger.log(logopts);
                                opts["back"]=true;
                                lbsts.exec(opts)
                                    .then(resolve);
                                break;
                            case lbsts.ENTRIES.checkout:
                                opts["state"] = {"name":lbsts.STATE_NAMES.amount};
                                logopts.msg="Transitioning from [" + lastState + "] to [" + opts["state"]["name"] +"]";
                                logger.log(logopts);
                                lbsts.exec(opts)
                                    .then(resolve);
                                break;
                            case lbsts.ENTRIES.placebet:
                                opts["state"] = {"name":lbsts.STATE_NAMES.placebet};
                                logopts.msg="Transitioning from [" + lastState + "] to [" + opts["state"]["name"] +"]";
                                logger.log(logopts);
                                lbsts.exec(opts)
                                    .then(resolve);
                                break;
                            case lbsts.ENTRIES.jackpot:
                                opts["state"] = {"name":lbsts.STATE_NAMES.jp_b1};
                                logopts.msg="Transitioning from [" + lastState + "] to [" + opts["state"]["name"] +"]";
                                opts["state"]["opts"] = opts["lastState"]["state"]["opts"] || {};
                                opts["state"]["opts"]["bet_type"] = "jackpot";
                                logger.log(logopts);
                                lbsts.exec(opts)
                                    .then(resolve);
                            break;
                            case lbsts.ENTRIES["6hw"]:
                                opts["state"] = {"name":lbsts.STATE_NAMES.fp_6hw_b1};
                                logopts.msg="Transitioning from [" + lastState + "] to [" + opts["state"]["name"] +"]";
                                logger.log(logopts);
                                lbsts.exec(opts)
                                    .then(resolve);
                            break;
                            case lbsts.ENTRIES["5aw"]:
                                opts["state"] = {"name":lbsts.STATE_NAMES.fp_5aw_b1};
                                logopts.msg="Transitioning from [" + lastState + "] to [" + opts["state"]["name"] +"]";
                                logger.log(logopts);
                                lbsts.exec(opts)
                                    .then(resolve);
                            break;
                            case lbsts.ENTRIES["4dr"]:
                                opts["state"] = {"name":lbsts.STATE_NAMES.fp_4dr_b1};
                                logopts.msg="Transitioning from [" + lastState + "] to [" + opts["state"]["name"] +"]";
                                logger.log(logopts);
                                lbsts.exec(opts)
                                    .then(resolve);
                            break;
                            case lbsts.ENTRIES.fatpay:
                                opts["state"] = {"name":lbsts.STATE_NAMES.fp_pb};
                                opts["state"]["opts"] = opts["lastState"]["state"]["opts"] || {};
                                opts["state"]["opts"]["bet_type"] = "fatpay";
                                logopts.msg="Transitioning from [" + lastState + "] to [" + opts["state"]["name"] +"]";
                                logger.log(logopts);
                                lbsts.exec(opts)
                                    .then(resolve);
                            break;
                            case lbsts.ENTRIES.cancel:
                                opts["state"] = {"name":lbsts.STATE_NAMES.cancel};
                                logopts.msg="Transitioning from [" + lastState + "] to [" + opts["state"]["name"] +"]";
                                logger.log(logopts);
                                lbsts.exec(opts)
                                    .then(resolve);
                            break;
                            default:
                                console.log("Transition : " + lastState + " >> " + nextState);
                                switch(lastState){
                                    case lbsts.STATE_NAMES.prediction:
                                        var prediction = opts["lastState"]["state"]["entries"][opts["user"]["in"]-1],
                                            cat = opts["lastState"]["state"]["opts"]["cat"],
                                            match = opts["lastState"]["state"]["opts"]["match"];

                                        console.log("Checking for ticket");
                                        if(opts["ticket"]){
                                            console.log("Ticket found");
                                            var maxMatch = 4;
                                            opts["ticket"]["no"]=opts["lastState"]["user"]["no"];
                                            opts["ticket"]["se"]=opts["lastState"]["user"]["se"];
                                            opts["ticket"][cat] = opts["ticket"][cat] || [];
                                            opts["ticket"][cat].push({match:match,prediction:prediction});
                                            console.log("Setting ticket : " + JSON.stringify(opts["ticket"]));
                                            opts["state"] = {"name":lbsts.STATE_NAMES.categories};
                                            console.log("Setting next state to : " + opts["state"]["name"]);
                                            lbsts.exec(opts)
                                                .then(resolve);
                                        }else{
                                            console.log("Ticket not found");
                                            opts["ticket"] = {};
                                            opts["ticket"]["no"]=opts["lastState"]["user"]["no"];
                                            opts["ticket"]["se"]=opts["lastState"]["user"]["se"];
                                            opts["ticket"][cat] = [];
                                            opts["ticket"][cat].push({match:match,prediction:prediction});
                                            console.log("Setting ticket : " + JSON.stringify(opts["ticket"]));
                                            opts["state"] = {"name":lbsts.STATE_NAMES.categories};
                                            console.log("Setting next state to : " + opts["state"]["name"]);
                                            lbsts.exec(opts)
                                                .then(resolve);
                                        }
                                        break;
                                    default :
                                        opts["state"] = {"name":nextState};
                                        console.log("Setting next state to : " + opts["state"]["name"]);
                                        if(lastState === lbsts.STATE_NAMES.jp_b3){
                                            opts["upd8ticket"] = "uo"
                                            lbsts.upd8ticket(opts)
                                                .then(lbsts.exec)
                                                .then(resolve)
                                        }else{
                                            lbsts.exec(opts)
                                                .then(resolve);
                                        }
                                    break;
                                }
                                break;
                        }

                    }
                break;
            }


        });
    };

    var cleanOpts = function (_opts) {
        return new Promise(function (resolve, reject) {
            var opts = _opts || {};
            if(!opts["input_wrong"]){
                opts["user"] = opts["lastState"]["user"]; // Set user fields

                if(opts["lastState"]["state"]["opts"] && !opts["state"]["opts"])
                {opts["state"]["opts"] = opts["lastState"]["state"]["opts"];}

                if(opts["lastState"]["state"]["ticket"] && !opts["state"]["ticket"])
                {opts["state"]["ticket"] = opts["lastState"]["state"]["ticket"];}

                delete opts["user"]["in"]; // Remove Previous state input
                delete opts["lastState"]; // Remove Previous state info
            }
            resolve(opts);
        });
    };

    var handleTransition = function (_opts) {
        return new Promise(function (resolve, reject) {
            var opts = _opts || {};
            transition(opts)
            .then(updateLastStateInDb)
            .then(cleanOpts)
            .then(resolve)

        });
    };

    var handleInput = function (_opts) {
        return new Promise(function (resolve, reject) {
            var opts = _opts || {};
            opts["back"]=false;
            console.log("Current route : " + opts["route"]);
            switch(opts["route"]){
                case "init":
                    opts["state"] = {"name":lbsts.STATE_NAMES.bet_types};
                    console.log("Setting next state to : " + opts["state"]["name"]);
                    lbsts.exec(opts)
                    .then(resolve);
                break;
                case "cont":
                    getLastState(opts)
                    .then(handleTransition)
                    .then(resolve);
                break;
                default:
                    resolve(opts);
                break;
            }

        });
    };

    var saveTxn = function (_opts) {
        return new Promise(function (resolve, reject) {
            var opts = _opts || {};
            if(!opts["input_wrong"]){
                opts["_sys_timestamps"]={};
                opts["_sys_timestamps"]["created_at"]=moment().toISOString();
                db["txns"].insert(opts,function(err,dbDoc){
                    console.log("Saved transaction to db");
                    resolve(opts);
                });
            }else{
                resolve(opts);
            }
        });
    };

    var buildScreenMenu = function (_opts) {
        return new Promise(function (resolve, reject) {

            var opts = _opts || {};
            opts["state"] = opts["state"] || {};
            if(opts["input_wrong"]){
                opts["state"] = opts["lastState"]["state"];
            }
            var
                menu = opts["state"]["prompt"] || "",
                delim = "$"
            ;

            opts["state"]["entries"] = opts["state"]["entries"] || [];
            opts["state"]["entries"].forEach(function(entry,idx){
                if(idx > 0){menu += "\n"};
                menu += (idx+1+(opts["screenoffset"] || 0)) + ". " + entry
            });

            menu = menu.replace(new RegExp("\n", 'g'),delim);
            opts["menu"]=menu;
            console.log("Setting menu : >>> " + menu + "\n <<<");
            resolve(opts);
        });
    };

    var handleReqeust = function (_opts) {
        return new Promise(function (resolve, reject) {
            var opts = _opts || {};
                handleInput(opts)
                .then(buildScreenMenu)
                .then(saveTxn)
                .then(resolve);
        });
    };

    buildTransitionTable();
    exports.handleRequest = handleReqeust;
}));
