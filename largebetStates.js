/**
 * Created by Bishaka on 05/04/2016.
 */
(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
        typeof define === 'function' && define.amd ? define(['exports'], factory) :
            (factory((global.lbsts = global.lbsts || {})));
}(this, function (exports) {
    'use strict';
    var Promise = require("bluebird"),
        request = require('request'),
        logger = require('./logger'),
        //matchUrl = "http://ls-largesol.rhcloud.com/Soccer12/api?topic=mktmatch",
        matchUrl = "http://www.pearlofafricaugandarally.com/out.json",
        jp_betUrl = "http://ls-largesol.rhcloud.com/Soccer12/api?topic=mbet",
        fp_betUrl = "http://ls-largesol.rhcloud.com/Soccer12/api?topic=fpay",
        CAT_OPTS = {
            "1 X 2":"1x2",
            "Under/Over":"uo",
            "Both to Score":"yn"
        },
        ENTRIES = {
            "back":"Back",
            "checkout":"Checkout",
            "placebet":"Place bet",
            "jackpot":"Jackpot",
            "fatpay":"Fatpay",
            "cancel":"Cancel",
            "6hw":"6HW: Odd 30",
            "5aw":"5AW: Odd 50",
            "4dr":"4DR: Odd 100"
        },
        STATE_NAMES = {
            "categories":"categories",
            "bet_types":"bet_types",
            "fp_pb":"fp_pb",
            "fp_6hw_b1":"fp_6hw_b1",
            "fp_6hw_b2":"fp_6hw_b2",
            "fp_6hw_b3":"fp_6hw_b3",
            "fp_5aw_b1":"fp_5aw_b1",
            "fp_5aw_b2":"fp_5aw_b2",
            "fp_5aw_b3":"fp_5aw_b3",
            "fp_4dr_b1":"fp_4dr_b1",
            "fp_4dr_b2":"fp_4dr_b2",
            "fp_4dr_b3":"fp_4dr_b3",
            "jp_b1":"jp_b1",
            "jp_b2":"jp_b2",
            "jp_b3":"jp_b3",
            "matches":"matches",
            "prediction":"prediction",
            "checkout":"checkout",
            "cancel":"cancel",
            "placebet":"placebet",
            "amount":"amount"
        };

    logger.engine.level='debug';
    var setPrompt = function (_opts) {
        return new Promise(function (resolve, reject) {
            var opts = _opts || {}, prompt = "";
                switch(opts["state"]["name"]){
                    case STATE_NAMES.fp_pb:
                        prompt = "\nPlayboards\n";
                    break;
                    case STATE_NAMES.bet_types:
                        prompt = "\nSoccer Twelve :\n";
                    break;
                    case STATE_NAMES.jp_b1:
                        prompt = "\nBoard: 1X2\n";
                    break;
                    case STATE_NAMES.fp_6hw_b1:
                    case STATE_NAMES.fp_6hw_b2:
                    case STATE_NAMES.fp_6hw_b3:
                        prompt = "\n6HW: Odd 30\n";
                    break;
                    case STATE_NAMES.fp_5aw_b1:
                    case STATE_NAMES.fp_5aw_b2:
                    case STATE_NAMES.fp_5aw_b3:
                        prompt = "\n5aw: Odd 50\n";
                    break;
                    case STATE_NAMES.fp_4dr_b1:
                    case STATE_NAMES.fp_4dr_b2:
                    case STATE_NAMES.fp_4dr_b3:
                        prompt = "\n4dr: Odd 100\n";
                    break;
                    case STATE_NAMES.jp_b2:
                        prompt = "\nBoard: BTS\n";
                        break;
                    case STATE_NAMES.jp_b3:
                        prompt = "\nBoard: U/O\n";
                        break;
                    case STATE_NAMES.cancel:
                        prompt = "\nBet ticket has been canceled\n";
                    break;
                    case STATE_NAMES.placebet:
                        prompt = "\nBet ticket has been submitted\n";
                    break;
                    case STATE_NAMES.categories:
                        prompt = "\nSelect category :\n";
                    break;
                    case STATE_NAMES.matches:
                        prompt = "\nSelect match :\n";
                    break;
                    case STATE_NAMES.prediction:
                        prompt = "\nSelect prediction :\n";
                    break;
                    case STATE_NAMES.amount:
                        prompt = "\nPlease enter bet stake ( Min 1,000 ):\n";
                    break;
                    case STATE_NAMES.checkout:
                        prompt =    "\nSoccer twelve has recieved payment advice of "+opts["ticket"]["amount"]+" for "+opts["lastState"]["user"]["no"]+".\n"
                    break;
                    default:

                    break;
                }
            opts["state"]["prompt"]=prompt;
            console.log("Setting prompt to : >>>" + opts["state"]["prompt"] + "<<<");
            resolve(opts);
        });
    };

    var getMatches = function (_opts) {
        return new Promise(function (resolve, reject) {
            var opts = _opts || {};
            request(matchUrl, function (error, response, matches) {
                if (!error && response.statusCode == 200) {
                    matches = JSON.parse(matches);
                    var idx = parseInt(opts["user"]["in"]), entry = opts["lastState"]["state"]["entries"][idx-1], entries = [], ids = [], cat = "";
                    if(opts["back"]){
                        cat = opts["lastState"]["state"]["opts"]["cat"];
                    }else{
                        cat = CAT_OPTS[entry.split("-")[0].trim()];
                    }

                    matches.forEach(function(match){
                        if(match["type"] === cat){
                            ids.push(match["id"]);
                            entries.push(match["home"] + " vs " + match["away"])
                        }
                    });
                }
                opts["state"]["opts"] = {cat:cat};
                opts["state"]["entries"]=entries;
                console.log("Setting options to : >>> " + JSON.stringify(opts["state"]["opts"])+" <<<");
                console.log("Setting entries to : >>> " + JSON.stringify(opts["state"]["entries"])+" <<<");
                resolve(opts);
            });
        });
    };

    var getPredictions = function (_opts) {
        return new Promise(function (resolve, reject) {
            var opts = _opts || {};
                opts["state"]["opts"] = opts["lastState"]["state"]["opts"];
                var cat = opts["lastState"]["state"]["opts"]["cat"],
                    entries = [],
                    idx = parseInt(opts["user"]["in"]),
                    match = opts["lastState"]["state"]["entries"][idx-1];
                switch(cat){
                    case CAT_OPTS["1 X 2"]:
                        entries = [
                            "1 - Home",
                            "x - Draw",
                            "2 - Away"
                        ];
                    break;
                    case CAT_OPTS["Under/Over"]:
                        entries = [
                            "u - Under",
                            "o - Over"
                        ];
                    break;
                    case CAT_OPTS["Both to Score"]:
                        entries = [
                            "y - Yes",
                            "n - No"
                        ];
                    break;
                }
            opts["state"]["opts"]["match"]=match;
            opts["state"]["entries"]=entries;
            console.log("Setting options to : >>> " + JSON.stringify(opts["state"]["opts"])+" <<<");
            console.log("Setting entries to : >>> " + JSON.stringify(opts["state"]["entries"])+" <<<");
            resolve(opts);
        });
    };

    var getCheckout = function (_opts) {
        return new Promise(function (resolve, reject) {
            var opts = _opts || {};
            opts["state"]["entries"] = [
                ENTRIES.placebet,
                ENTRIES.cancel,
            ];
            resolve(opts);
        });
    }

    var getCategories = function (_opts) {
        return new Promise(function (resolve, reject) {
            var opts = _opts || {}, entries = [];
            opts["ticket"] = opts["ticket"] || {};
            var _l1 = opts["ticket"][CAT_OPTS["1 X 2"]] || [],
                _l2 = opts["ticket"][CAT_OPTS["Both to Score"]] || [],
                _l3 = opts["ticket"][CAT_OPTS["Under/Over"]] || [],
                l1 = _l1.length,
                l2 = _l2.length,
                l3 = _l3.length;

            entries = [
                (l1 > 0)?"1 X 2 - ("+l1+")":"1 X 2",
                (l2 > 0)?"Both to Score - ("+l2+")":"Both to Score",
                (l3 > 0)?"Under/Over - ("+l3+")":"Under/Over",
            ];
            opts["state"]["entries"]=entries;
            console.log("Setting entries to : " + JSON.stringify(opts["state"]["entries"]));
            resolve(opts);

            resolve(opts);
        });
    };

    var placeBet = function (_opts) {
        return new Promise(function (resolve, reject) {
            var opts = _opts || {},
                ticket = opts["ticket"];


                opts["ticket"]["_1x2"] = opts["ticket"]["1x2"];
                opts["ticket"]["_6hw"] = opts["ticket"]["6hw"];
                opts["ticket"]["_5aw"] = opts["ticket"]["5aw"];
                opts["ticket"]["_4dr"] = opts["ticket"]["4dr"];
                opts["ticket"]["se"] = opts["user"]["se"];
                ticket = opts["ticket"];

            var no = opts["ticket"]["no"] + "";

            if(no.charAt(0) === '0'){
                opts["ticket"]["no"] = "256"+no.substr(1);
            }


            var pkg= JSON.stringify(ticket);

            console.log("Package : " + pkg);

            switch(opts["lastState"]["state"]["opts"]["bet_type"]){
                case "jackpot":
                    request.post({url:jp_betUrl, form: {ticket:pkg}}, function(err,response,body){
                        console.log("Response : " + JSON.stringify(response));
                        resolve(opts);
                    });
                    break;
                case "fatpay":
                    request.post({url:fp_betUrl, form: {ticket:pkg}}, function(err,response,body){
                        console.log("Response : " + JSON.stringify(response));
                        resolve(opts);
                    });
                    break;
            }
        });
    };

    var getBetTypes = function (_opts) {
        return new Promise(function (resolve, reject) {
            var opts = _opts || {}, entries = [];
            request(matchUrl, function (error, response, matches) {
                if (!error && response.statusCode == 200) {
                    matches = JSON.parse(matches);
                    opts["state"]["opts"]= opts["state"]["opts"] || {};
                    opts["state"]["opts"]["v2"] = opts["state"]["opts"]["v2"] || {};
                    opts["state"]["opts"]["v2"]["matches"] = matches;
                }
                console.log("Fetched matches : " + JSON.stringify(matches));
                entries = [
                    ENTRIES.jackpot,
                    ENTRIES.fatpay,
                ];
                opts["state"]["entries"]=entries;
                console.log("Setting entries to : " + JSON.stringify(opts["state"]["entries"]));
                resolve(opts);
            });


        });
    };

    var upd8ticket = function (_opts) {
        return new Promise(function (resolve, reject) {
            var opts = _opts || {};
                var inputs = opts["user"]["in"].split(","),
                    cat = opts["upd8ticket"] || "n/a",
                    entries = opts["lastState"]["state"]["entries"];

            if(cat != "n/a") {
                opts["ticket"] = opts["ticket"] || {};
                opts["ticket"]["type"] = cat;
                opts["ticket"]["no"]= opts["lastState"]["user"]["no"];
                opts["ticket"][cat] = opts["ticket"][cat] || [];
                inputs.forEach(function(input){
                    var tokens = input.trim().split(' '),
                        match_id = parseInt(tokens[0]),
                        match_pr = tokens[1];

                    match_id = match_id%4;
                    if(match_id === 0){
                        match_id = 4;
                    }
                    match_id--;
                    var match = {match:entries[match_id],prediction:match_pr};
                    opts["ticket"][cat].push(match)
                })
            }
            resolve(opts);
        });
    };

    var getBoardEntries = function (_opts) {
        return new Promise(function (resolve, reject) {
            var opts = _opts || {}, entries = [];
                var matches = opts["lastState"]["state"]["opts"]["v2"]["matches"];
                switch(opts["state"]["name"]){
                    case STATE_NAMES.fp_6hw_b1:
                    case STATE_NAMES.fp_6hw_b2:
                    case STATE_NAMES.fp_6hw_b3:
                    case STATE_NAMES.fp_5aw_b1:
                    case STATE_NAMES.fp_5aw_b2:
                    case STATE_NAMES.fp_5aw_b3:
                    case STATE_NAMES.fp_4dr_b1:
                    case STATE_NAMES.fp_4dr_b2:
                    case STATE_NAMES.fp_4dr_b3:
                        var offset = ((parseInt(opts["state"]["name"].slice(-1))) - 1);

                        for(var i = 0; i < 4;i++){
                            var idx = i + (4*offset);
                            entries.push(matches[i]["home"] + " vs " + matches[i]["away"])
                        }

                        opts["state"]["entries"]=entries;
                        opts["screenoffset"] = offset*4;
                        console.log("Setting entries to : >>> " + JSON.stringify(opts["state"]["entries"])+" <<<");
                        resolve(opts);
                    break;
                    case STATE_NAMES.fp_pb:
                        entries = [
                            ENTRIES["6hw"],
                            ENTRIES["5aw"],
                            ENTRIES["4dr"]
                        ]
                        opts["state"]["entries"]=entries;
                        console.log("Setting entries to : >>> " + JSON.stringify(opts["state"]["entries"])+" <<<");
                        resolve(opts);
                    break;
                    case STATE_NAMES.jp_b1:
                        matches.forEach(function(match){
                            if(match["type"] === "1x2"){
                                entries.push(match["home"] + " vs " + match["away"])
                            }
                        });
                        opts["state"]["entries"]=entries;
                        console.log("Setting entries to : >>> " + JSON.stringify(opts["state"]["entries"])+" <<<");
                        resolve(opts);
                    break;
                    case STATE_NAMES.jp_b2:
                        matches.forEach(function(match){
                            if(match["type"] === "yn"){
                                entries.push(match["home"] + " vs " + match["away"])
                            }
                        });
                        opts["state"]["entries"]=entries;
                        opts["screenoffset"] = 4*1;
                        opts["upd8ticket"] = "1x2";
                        console.log("Setting entries to : >>> " + JSON.stringify(opts["state"]["entries"])+" <<<");
                        upd8ticket(opts).then(resolve)
                    break;
                    case STATE_NAMES.jp_b3:
                        matches.forEach(function(match){
                            if(match["type"] === "uo"){
                                entries.push(match["home"] + " vs " + match["away"])
                            }
                        });

                        opts["state"]["entries"]=entries;
                        opts["screenoffset"] = 4*2;
                        opts["upd8ticket"] = "yn";
                        console.log("Setting entries to : >>> " + JSON.stringify(opts["state"]["entries"])+" <<<");
                        upd8ticket(opts).then(resolve)
                    break;
                }
        });
    };

    var setEntries = function (_opts) {
        return new Promise(function (resolve, reject) {
            var opts = _opts || {}, entries = [];
            switch(opts["state"]["name"]){
                case STATE_NAMES.jp_b1:
                case STATE_NAMES.jp_b2:
                case STATE_NAMES.jp_b3:
                case STATE_NAMES.fp_pb:
                case STATE_NAMES.fp_6hw_b1:
                case STATE_NAMES.fp_6hw_b2:
                case STATE_NAMES.fp_6hw_b3:
                case STATE_NAMES.fp_5aw_b1:
                case STATE_NAMES.fp_5aw_b2:
                case STATE_NAMES.fp_5aw_b3:
                case STATE_NAMES.fp_4dr_b1:
                case STATE_NAMES.fp_4dr_b2:
                case STATE_NAMES.fp_4dr_b3:
                    getBoardEntries(opts)
                        .then(resolve);
                break;
                case STATE_NAMES.bet_types:
                    getBetTypes(opts)
                        .then(resolve);
                break;
                case STATE_NAMES.categories:
                    getCategories(opts)
                    .then(resolve);
                break;
                case STATE_NAMES.matches:
                    getMatches(opts)
                    .then(resolve);
                break;
                case STATE_NAMES.placebet:
                    placeBet(opts)
                        .then(resolve);
                break;
                case STATE_NAMES.prediction:
                    getPredictions(opts)
                    .then(resolve);
                break;
                case STATE_NAMES.checkout:
                    getCheckout(opts)
                        .then(resolve);
                    break;
                default:
                    resolve(opts);
                break;
            }
        });
    };

    var setCheckoutEntry = function (_opts) {
        return new Promise(function (resolve, reject) {
            var opts = _opts || {}, logopts = {level:"debug",file:getFilename(),func:"setCheckoutEntry", id:"[user_sess]["+opts["user"]["se"]+"]"};
            logopts.msg="Checking for ticket";
            logger.log(logopts);
            switch(opts["state"]["name"]){
                case STATE_NAMES.checkout:
                case STATE_NAMES.cancel:
                case STATE_NAMES.placebet:
                case STATE_NAMES.amount:
                break;
                default:
                    if(opts["ticket"]){
                        logopts.msg="Found ticket";
                        logger.log(logopts);

                        var _l1 = opts["ticket"][CAT_OPTS["1 X 2"]] || [],
                            _l2 = opts["ticket"][CAT_OPTS["Under/Over"]] || [],
                            _l3 = opts["ticket"][CAT_OPTS["Both to Score"]] || [],
                            l1 = _l1.length,
                            l2 = _l2.length,
                            l3 = _l3.length;

                        logopts.msg="Lengths calculated : "+l1+", "+l2+", "+l3+"";
                        logger.log(logopts);

                        if( l1 === l2 && l2 === l3 && l1 > 0){
                            logopts.msg="Adding checkout entry";
                            logger.log(logopts);
                            opts["state"]["entries"].push(ENTRIES.checkout);
                        }
                    }
                break;
            }

            resolve(opts);
        });
    };

    var setBackEntry = function (_opts) {
        return new Promise(function (resolve, reject) {
            var opts = _opts || {};
            var opts = _opts || {}, logopts = {level:"debug",file:getFilename(),func:"setBackEntry", id:"[user_sess]["+opts["user"]["se"]+"]"};
            logopts.msg="Checking state : " + opts["state"]["name"];
            logger.log(logopts);
            switch(opts["state"]["name"]){
                case STATE_NAMES.categories:
                case STATE_NAMES.checkout:
                case STATE_NAMES.cancel:
                case STATE_NAMES.placebet:
                case STATE_NAMES.amount:
                case STATE_NAMES.bet_types:
                case STATE_NAMES.jp_b1:
                case STATE_NAMES.jp_b2:
                case STATE_NAMES.jp_b3:
                case STATE_NAMES.fp_pb:
                case STATE_NAMES.fp_6hw_b1:
                case STATE_NAMES.fp_6hw_b2:
                case STATE_NAMES.fp_6hw_b3:
                case STATE_NAMES.fp_5aw_b1:
                case STATE_NAMES.fp_5aw_b2:
                case STATE_NAMES.fp_5aw_b3:
                case STATE_NAMES.fp_4dr_b1:
                case STATE_NAMES.fp_4dr_b2:
                case STATE_NAMES.fp_4dr_b3:
                break;
                default:
                    logopts.msg="Adding back entry";
                    logger.log(logopts);
                    opts["state"]["entries"] = opts["state"]["entries"] || [];
                    opts["state"]["entries"].push(ENTRIES.back);
                break;
            }

            resolve(opts);
        });
    };

    var state = function (_opts) {
        return new Promise(function (resolve, reject) {
            var opts = _opts || {};
            console.log("Executing State : " + opts["state"]["name"]);
                setPrompt(opts)
                .then(setEntries)
                .then(setCheckoutEntry)
                .then(setBackEntry)
                .then(resolve);
        });
    };

    var getFilename = function () {
           var src = "";
           try{
                throw new Error();
           }catch(err){
               var regex = /\(.*\)/,
                   match = regex.exec(err.stack),
                   name = match[0].replace("(","").replace(")",""),
                   tokens = name.split(require('path').sep);
                   src = tokens[tokens.length-1].split(":")[0];
           }
        return src;
    };

    exports.exec = state;
    exports.upd8ticket = upd8ticket;
    exports.STATE_NAMES = STATE_NAMES;
    exports.ENTRIES = ENTRIES;
}));
