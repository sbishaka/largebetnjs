/**
 * Created by Bishaka on 05/04/2016.
 */
(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
        typeof define === 'function' && define.amd ? define(['exports'], factory) :
            (factory((global.db = global.db || {})));
}(this, function (exports) {
    'use strict';
    var
        dbstore = require("nedb"),
        db_dir = process.env.OPENSHIFT_DATA_DIR || ".",
        db = {
            "txns":new dbstore({filename:db_dir + "/largebetnjs_db/txns.json",autoload:true})
        }
    ;
    exports.db = db;
}));
